<?php
/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
class DB extends mysqli{
    
//private $hostname = "localhost";
//private $username = "iitcs425";
//private $password = "password";
//private $dbname = "iitcs425_MyNewDatabase";

private $hostname = "127.0.0.1";
private $username = "root";
private $password = "admin";
private $dbname = "MyNewDatabase";


private static $instance = null;

//This method must be static, and must return an instance of the object if the object
 //does not already exist.
 public static function getInstance() {
   if (!self::$instance instanceof self) {
     self::$instance = new self;
   }
   return self::$instance;
 }

 // The clone and wakeup methods prevents external instantiation of copies of the Singleton class,
 // thus eliminating the possibility of duplicate objects.
 public function __clone() {
   trigger_error('Clone is not allowed.', E_USER_ERROR);
 }
 public function __wakeup() {
   trigger_error('Deserializing is not allowed.', E_USER_ERROR);
 }
 
 // private constructor
private function __construct() {
    parent::__construct($this->hostname, $this->username, $this->password, $this->dbname);
    if (mysqli_connect_error()) {
        exit('Connect Error (' . mysqli_connect_errno() . ') '
                . mysqli_connect_error());
    }
    parent::set_charset('utf-8');
}


public function search($name){
    
    $result = $this->query("CALL search('$name');");
    if(mysqli_num_rows($result) < 1) {
                exit("Person not found. Type first and last name.");
               
            }
            else{
            while($row3 = mysqli_fetch_assoc($result)){
                echo "First Name: ".$row3['v_firstName']."<br>";
                echo "Last Name: ".$row3['v_lastName']."<br>";
                echo "Email: ".$row3['v_email']."<br>";
                echo "Degree Type: ".$row3['v_degreeType']."<br>";
                echo "Degree Status: ".$row3['v_degreeStatus']."<br>";
                echo "Year/Semester Joined IIT: ".$row3['v_semester']." ".$row3['v_year']."<br>";
                
                echo "GPA: ".$row3['v_gpa']."<br>";
                echo "Interest Groups/Courses: ".$row3['v_ig']."<br>";
            }
            }
            mysqli_free_result($result);
}
        
        public function verifyLogin($username,$password){
            $checkLogin = $this->query("SELECT 1 FROM LogIn WHERE username = '$username' AND password = '$password'");
            return $checkLogin->data_seek(0);
        }
public function convertHTML($var){
    return $this->real_escape_string($var);
}

public function loginGenerator($job, $firstName, $lastName, $email, $yearJoined, $password){
 $this->query("CALL register('$job','$firstName','$lastName','$email',$yearJoined, '$password')");
 $this->query("COMMIT");
 return;
}

public function make_links_clickable($text,$url){
    return preg_replace('!(((f|ht)tp(s)?://)[-a-zA-Zа-яА-Я()0-9@:%_+.~#?&;//=]+)!i', '<a href="'.$url.'" target = "_blank">'.$text.'</a>', $url);
}


  public function getForum($userId){
    return $this->query("SELECT name FROM InterestGroup ig"
        . " INNER JOIN EnrollmentIG e"
        . " ON e.interestGroup = ig.name"
        . " WHERE e.id = $userId;");
}

public function getForumTitle($interestGroup){
    return $this->query("SELECT title FROM ForumTitle WHERE interestGroup = '$interestGroup'");
  
}

public function getComments($title, $interestGroup){
    return $this->query("SELECT comments, firstName, lastName "
            . "FROM ForumComments c INNER JOIN ForumTitle t "
            . "ON c.titleId = t.id "
            . "INNER JOIN Users u "
            . "ON u.id = c.userId "
            . "WHERE title = '$title' AND interestGroup = '$interestGroup' "
            . "ORDER by c.id");
}

public function linkToInterestGroups($interestGroup){
     return ('<a href="interestGroup.php?name='.$interestGroup.'">'.$interestGroup.'</a>');
}
public function linkToTitle($title){
    return ('<a href="forumTitle.php?name='.$title.'" >'.$title.'</a>');
}

public function linkToForumSearchResults($title, $ig){
    return ('<a href="forumSearchResults.php?name='.$title.'?ig='.$ig.'" >'.$ig.' - '.$title.'</a>');
}

public function credentials($username){
  return $this->query("SELECT u.id as userId, firstName, lastName, job "
      . "FROM Users u "
      . "INNER JOIN LogIn l "
      . "ON u.id = l.id "
      . "WHERE username = '$username' ");
}

public function createPost($title, $userId, $interestGroup, $comment){
  $this->query("INSERT INTO ForumTitle(title, userId, interestGroup) VALUES"
      . " ('$title', $userId, '$interestGroup')");
  $this->query("COMMIT");
  $titleId = $this->query("SELECT id FROM ForumTitle"
      . " WHERE title = '$title' AND userId = $userId AND interestGroup = '$interestGroup'");
  while($row = mysqli_fetch_assoc($titleId)){
    $tid = $row['id'];
  }
  $comments = $this->query("INSERT INTO ForumComments(titleId, comments, userId) VALUES"
      . " ($tid, '$comment', $userId)");
}

public function getProfile($userId, $job){
  if($job == 'STUDENT'){
    $profile = $this->query("SELECT firstName, lastName, email, degreeType, degreeStatus, year, "
        . "semester, gpa FROM Users u INNER JOIN Student s ON s.userId = u.id "
        . "WHERE userId = $userId");
    $interestGroups = $this->query("SELECT interestGroup FROM EnrollmentIG"
        . " WHERE id = $userId");
            while($row3 = mysqli_fetch_assoc($profile)){
                echo "First Name: ".$row3['firstName']."<br>";
                echo "Last Name: ".$row3['lastName']."<br>";
                echo "Email: ".$row3['email']."<br>";
                echo "Degree Type: ".$row3['degreeType']."<br>";
                echo "Degree Status: ".$row3['degreeStatus']."<br>";
                echo "Year/Semester Joined IIT: ".$row3['semester']." ".$row3['year']."<br>";
                echo "GPA: ".$row3['gpa']."<br>";
            }
            mysqli_free_result($profile);
            echo "Interest Groups / Courses: ";
            while($row = mysqli_fetch_assoc($interestGroups)){
              echo $row['interestGroup']." ";
            }
            echo "<br>";
            
  } 
//  if($job == 'PROFESSOR'){
//    $profile = $this->
//  }
}

//public function getCourseType(){
//  return $this->query("SELECT courseType FROM Course");
//}
//
//public function getCourseNumber($courseType){
//  return $this->query("SELECT courseNumber FROM Course WHERE courseType = '.$courseType.'");
//}

public function enroll($id, $courseType, $courseNumber){
  $userId = $id;
  $check = $this->query("SELECT id FROM Course WHERE courseType = '$courseType' "
      . "AND courseNumber = $courseNumber");
  if(mysqli_num_rows($check) < 1){
    $this->query("INSERT INTO Course(courseType, courseNumber) VALUES ('$courseType',$courseNumber)");
    $this->query("Call makeInterestGroups()");
    DB::getInstance()->enroll($userId, $courseType, $courseNumber);
  }
  else {
    $interestGroup = $courseType.$courseNumber;
    while($row = mysqli_fetch_assoc($check)){
    $courseId = $row['id'];
    $this->query("INSERT INTO Enrollment1(id, courseId) VALUES($id, $courseId)");
    $this->query("INSERT INTO EnrollmentIG VALUES($id,'$interestGroup')");
    }
  }
}
 
public function currPrivacy($id){
  return $this->query("SELECT gpa, interestGroup FROM Privacy WHERE id = $id)");
}
public function privacy($id, $gpaPrivacy, $igPrivacy){
  $this->query("CALL changePrivacy($id, $gpaPrivacy, $igPrivacy)");
}

public function reply($title, $userId, $ig, $message){
  $titleId = $this->query("SELECT DISTINCT(t.id) FROM ForumComments c INNER JOIN ForumTitle t "
      . "WHERE title = '$title' AND interestGroup = '$ig'");
  while($row = mysqli_fetch_assoc($titleId)){
    $tid = $row['id'];
  }
  $this->query("INSERT INTO ForumComments(titleId, comments, userId) VALUES($tid, '$message', $userId)");
}

public function student($year,$semester,$degreeType,$userId){
  return $this->query("INSERT INTO Student(year, semester, degreeType, userId) VALUES ($year, '$semester', "
      . " '$degreeType', $userId)");
}

public function professor($year, $position, $userId){
  return $this->query("INSERT INTO Professor(year, position, userId) VALUES($year, '$position', $userId)");
}

public function getUserId($firstName, $lastName){
  return $this->query("SELECT id FROM Users where firstName = '$firstName' AND lastName = '$lastName'");
}


        public function verifyAdmin($username,$password){
            $checkAdmin = $this->query("SELECT 1 FROM LogIn l INNER JOIN Users u ON u.id = l.id INNER JOIN Admin a ON u.id = a.adminId WHERE username = '$username' AND password = '$password'");
            return $checkAdmin->data_seek(0);
        }
        

public function selectAll($tableName){
  return $this->query("SELECT * FROM $tableName");
}

public function getTable($tableName, $id){
  return $this->query("SELECT * FROM $tableName WHERE id = $id");
}

public function describe($tableName){
  return $this->query("DESCRIBE $tableName");
}
public function insertAdmin($adminId){
  $this->query("INSERT INTO Admin VALUES ($adminId)");
}

public function forumSearch($string){
  return $this->query("CALL forumSearch('$string')");
}

public function students(){
  return $this->query("SELECT firstName, lastName, s.id FROM Users u INNER JOIN"
      . " Student s ON u.id = s.userId");
}

public function assignMod($userId, $courseId){
  $this->query("CALL assignModerators($userId, $courseId)");
}

public function courses(){
  return $this->query("SELECT id, courseType, courseNumber FROM Course");
}

public function users(){
  return $this->query("SELECT id, firstName, lastName FROM Users");
}

public function assignTA($courseId, $studentId){
//  $assignTA = $this->query("CALL addTA('$firstName','$lastName','$courseType',$courseNumber)");
//  $studentName = $firstName." ".$lastName;
//  $courseName = $courseType." ".$courseNumber;
//  if(!$assignTA){
//    echo $studentName.' cannot be assigned TA to '.$courseName.' because he/she is enrolled in this course';
//  } else {
//    echo $studentName." is now a TA of ".$courseName."<br>";
//  }
  $assignTA = $this->query("CALL addTA($courseId, $studentId)");
  if(!$assignTA){
    echo 'Student cannot be assigned TA because he/she is enrolled in this course';
  } else {
    echo 'Student assigned as TA';
  }
}

}
       

