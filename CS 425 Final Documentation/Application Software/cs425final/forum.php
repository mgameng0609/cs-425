<?php session_start() ?>
<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <link rel="stylesheet" type="text/css" href="style.css">
        <title>Forum</title>
    </head>
    <body>
        <h1><?php include "home.php"; ?> </h1>
        <br>
        <form action="forum.php" method = "POST">
            <select name="filterOption">
                <option>Most popular</option>
                <option>Your most viewed</option>
            </select>
            <input type="submit" value="Filter" name="filter" />
        </form>
        <br>
        <form action="forumSearch.php" method="POST">
            <input type="text" name="forumSearch" value="" />
            <input type="submit" value="Search" name="Search" />
        </form>
        <br>
        <table border="1">
            <thead>
                <tr>
                    <th>Interest Groups / Club / Course</th>
                </tr>
            </thead>
            <tbody>
        <?php
           require_once("Database.php");
           if($_SERVER['REQUEST_METHOD'] == "POST"){
             
             if($_POST['filterOption'] == 'Most popular'){
               $server = 'mostPopular';
             } 
             if($_POST['filterOption'] == 'Your most viewed'){
             $server = 'mostViewed';
             }
           } else {
             $server = 'norm';
           }
//           echo $server;
           $forum = DB::getInstance()->getForum($_SESSION['userId']);
           while($row = mysqli_fetch_assoc($forum)){

                  ?>
                      <tr>
                          <td>
                      <?php

                    $interestGroup = DB::getInstance()->linkToInterestGroups($row['name']);
                    echo $interestGroup;
                    ?>
                        </td>
                    </tr>
                <?php
           }

        ?>
            </tbody>
        </table>
    </body>
</html>
