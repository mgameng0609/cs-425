<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <link rel="stylesheet" type="text/css" href="style.css">
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <form action="generateLogin.php">
            <p>Student or Professor: <select name="job">
                <option>Student</option>
                <option>Professor</option>
                </select></p>
            <p>Department: <select name="degreeType">
                <option>ARCH</option>
                <option>AAH</option>
                <option>BIOL</option>
                <option>BUS</option>
                <option>CHEM</option>
                <option>CAE</option>
                <option>COM</option>
                <option>CS</option>
                <option>ECON</option>
                <option>ECE</option>
                <option>EGM</option>
                <option>GLS</option>
                <option>HIST</option>
                <option>HUM</option>
                <option>ITM</option>
                <option>IPRO</option>
                <option>LAW</option>
                <option>LIT</option>
                <option>MATH</option>
                <option>PHIL</option>
                <option>PHYS</option>
                <option>PSYCH</option>
                <option>POLI</option>
                <option>SOC</option>
                <option>SSCI</option>
                </select></p>
            <p>Semester: <select name="semester">
                <option>Spring</option>
                <option>Summer</option>
                <option>Fall</option>
                </select></p>
            <p>First Name: <input type="text" size="20" name="firstName"></p>
            <p>Last Name: <input type="text" size="20" name="lastName"></p>
            <p>Email: <input type="text" size="20" name="email"></p>
            <p>Year joined IIT: <input type="text" size="20" name="yearJoined"></p>
            <p>Password: <input type="password" name="password" value="" size="20" /></p>
            <p>Confirm Password: <input type="password" name="confirmPassword" value="" /></p>
            <input type="submit" value="Register" />
        </form>
    </body>
</html>
