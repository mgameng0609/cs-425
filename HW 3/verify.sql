CREATE DEFINER=`root`@`localhost` PROCEDURE `verify`(
IN userId		INT,
IN comments		VARCHAR(320),
IN title		VARCHAR(320),
IN titleId		INT)
BEGIN
	/* checks if the user is part of the InterestGroup */
	IF (userID IN (SELECT id FROM EnrollmentIG e
						WHERE e.interestGroup = (SELECT interestGroup FROM ForumTitle f
												 WHERE f.title = title)))
		THEN 
			INSERT INTO ForumComments VALUES(titleId, comments, userId); 
		ELSE
        SELECT 'Error: User is not a member of this group. Unauthorized to post.';
	END IF;
END