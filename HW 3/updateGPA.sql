CREATE DEFINER=`root`@`localhost` TRIGGER `updateGPA` AFTER UPDATE ON `Enrollment1` FOR EACH ROW
BEGIN
	DECLARE done INT DEFAULT FALSE;
	DECLARE totalGPA INT;
    DECLARE newGPA	DECIMAL(3,2);
    DECLARE totalCredits	INT;
    DECLARE v_grade		CHAR;
    DECLARE v_gradeINT	INT;
    DECLARE v_credit	INT;
    DECLARE cur1 CURSOR FOR
			SELECT grade, credit
            FROM Enrollment1
            WHERE id = NEW.id;
	DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;
    
    SET totalCredits = (SELECT SUM(credit)
						FROM Enrollment1
                        WHERE id = NEW.id);
	SET totalGPA = 0;
	
	OPEN cur1;
    calc: LOOP
		FETCH cur1 INTO v_grade, v_credit;
        IF done
        THEN LEAVE calc;
        END IF;
        
        IF v_grade = 'A' THEN SET v_gradeINT = 4; END IF;
        IF v_grade = 'B' THEN SET v_gradeINT = 3; END IF;
        IF v_grade = 'C' THEN SET v_gradeINT = 2; END IF;
        IF v_grade = 'D' THEN SET v_gradeINT = 1; END IF;
        IF v_grade = 'E' THEN SET v_gradeINT = 0; END IF;
        
        SET totalGPA = totalGPA + (v_gradeINT * v_credit);
	END LOOP;
    CLOSE cur1;
		SET newGPA = totalGPA / totalCredits;
        UPDATE Student
        SET gpa = newGPA
        WHERE id = NEW.id;
    
END