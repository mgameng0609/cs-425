CREATE DEFINER=`root`@`localhost` PROCEDURE `addTA`(
IN v_courseId	INT,
IN v_sid		INT)
BEGIN
    INSERT INTO CourseTA VALUES (v_courseId, v_sid);
END