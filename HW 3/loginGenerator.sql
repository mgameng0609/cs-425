CREATE DEFINER=`root`@`localhost` PROCEDURE `loginGenerator`(
IN firstName	VARCHAR(20),
IN lastName	VARCHAR(40),
IN joinedYear  INT,
IN password	   VARCHAR(255),
OUT v_id	INT)
BEGIN
    DECLARE v_first		CHAR;
    DECLARE v_login		VARCHAR(20);
    
    SET v_first = LEFT(firstName, 1); /* gets first letter of first name */
    
    /* generates the login username combination */
    SET v_login = LOWER(CONCAT(v_first, lastName, joinedYear));
    
    SELECT 'Your login username is:', v_login;
    
    /* keeps the LogIn(id) from increasing each new person registered */
    IF (SELECT MAX(id) FROM LogIn) is NULL
    THEN SET v_id = 1;
    ELSE (SELECT (MAX(id) + 1) INTO v_id FROM LogIn);
    END IF;
    
    SELECT v_id;
    
    INSERT INTO LogIn VALUES (v_login, password, v_id);
END