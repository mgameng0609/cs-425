CREATE DEFINER=`root`@`localhost` PROCEDURE `makeInterestGroups`()
BEGIN
	DECLARE done INT DEFAULT FALSE;
	DECLARE v_courseType		VARCHAR(20);
    DECLARE v_courseNumber	INT;
    DECLARE name1			VARCHAR(100);
	DECLARE cur1 CURSOR FOR 
		SELECT courseType, courseNumber 
        FROM Course
        WHERE interestGroup IS NULL;
	DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;
    
	OPEN cur1;
    makeIG: LOOP
		FETCH cur1 into v_courseType, v_courseNumber;
        
        IF done
        THEN LEAVE makeIG;
        END IF;
        
        /* generates the interest group name from course type and number */
        SET name1 = CONCAT(v_courseType, v_courseNumber);
        
        /* checks if this interest group name is in InterestGroup */
        IF name1 NOT IN (SELECT name FROM InterestGroup WHERE name =  name1)
        THEN
        INSERT INTO InterestGroup(name) VALUE(name1);
        END IF;
        
	END LOOP;
    CLOSE cur1;
END