CREATE TABLE LogIn(
username      VARCHAR(20)     PRIMARY KEY,
password      VARCHAR(80)     NOT NULL,
id            INT             UNIQUE);

CREATE TABLE Users(
id            INT             PRIMARY KEY REFERENCES LogIn(id),
firstName     VARCHAR(40)     NOT NULL,
lastName      VARCHAR(40)     NOT NULL,
email         VARCHAR(80)     UNIQUE,
job           VARCHAR(20)     NOT NULL);

CREATE TABLE Student(
id            INT             PRIMARY KEY,
year          INT             NOT NULL,
semester      VARCHAR(10)     NOT NULL,
gpa           INT       NOT NULL,
degreeType    VARCHAR(40)     NOT NULL,
degreeStatus  VARCHAR(20)     NOT NULL,
points	      INT		DEFAULT 0,
userId        INT REFERENCES      Users(id));

CREATE TABLE TA(
id            INT             ,
studentId     INT REFERENCES      Student(id),
CONSTRAINT pkc_ta PRIMARY KEY (id, studentId));

CREATE TABLE Professor(
id            INT             PRIMARY KEY,
year          INT             NOT NULL,
position      VARCHAR(40)     NOT NULL,
projects      VARCHAR(200),
experience    VARCHAR(200)    NOT NULL,
userId        INT REFERENCES      Users(id));

CREATE TABLE Course(
id            INT             PRIMARY KEY,
courseType    VARCHAR(10)     NOT NULL,
courseNumber  INT             NOT NULL,
profId        INT NOT NULL REFERENCES      Professor(id),
interestGroup VARCHAR(40) REFERENCES      InterestGroup(name),
semester      VARCHAR(10)     NOT NULL,
year          INT             NOT NULL,
avgGPA        INT);

CREATE TABLE InterestGroup(
name          VARCHAR(40)     PRIMARY KEY,
category      VARCHAR(40)  ,
purpose       VARCHAR(40)  ,
events        VARCHAR(100));   

CREATE TABLE Enrollment1(
id            INT REFERENCES      Student(id),
courseId      INT REFERENCES      Course(id),
grade		  CHAR		NOT NULL,
credit		  INT,
CONSTRAINT    pkc_enrollment1 PRIMARY KEY(id, courseId));

CREATE TABLE EnrollmentIG(
id            INT REFERENCES      Users(id),
interestGroup VARCHAR(40) REFERENCES      InterestGroup(name),
CONSTRAINT    pkc_enrollmentig  PRIMARY KEY(id, interestGroup));

CREATE TABLE JobList(
id            INT PRIMARY KEY REFERENCES      Student(id),
company       VARCHAR(40),
position      VARCHAR(40),
type          VARCHAR(40),
description   VARCHAR(100));

CREATE TABLE ForumTitle(
id				INT PRIMARY KEY ,
title			VARCHAR(320),
interestGroup	VARCHAR(50) REFERENCES InterestGroup(name),
userId			INT REFERENCES Users(id));

CREATE TABLE ForumComments(
titleId			INT REFERENCES ForumTitle(id),
comments		VARCHAR(320) NOT NULL,
userId			INT REFERENCES Users(id));

CREATE TABLE Privacy(
id				INT PRIMARY KEY REFERENCES Student(id),
gpa				BOOL DEFAULT 0,
interestGroup	BOOL DEFAULT 0);

CREATE TABLE CourseTA(
id				INT REFERENCES Course(id),
TAId			INT REFERENCES TA(id),
CONSTRAINT pkc_courseta PRIMARY KEY (id, TAId));




