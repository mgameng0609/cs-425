CREATE DEFINER=`root`@`localhost` TRIGGER checkAddTA BEFORE INSERT ON CourseTA
FOR EACH ROW
BEGIN
   IF NEW.TAId IN (SELECT id FROM Enrollment1 WHERE courseId = NEW.id)
   THEN SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = 'This student is currently enrolled in this course.';
   END IF;
END