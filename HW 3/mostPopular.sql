CREATE DEFINER=`root`@`localhost` PROCEDURE `mostPopular`(
IN ig VARCHAR(50))
BEGIN
	SELECT t.title, COUNT(comments) as numPosts
    FROM ForumTitle t
    INNER JOIN ForumComments c
    ON c.titleId = t.id
    WHERE interestGroup = ig
    GROUP by t.title
    ORDER BY numPosts DESC;
END