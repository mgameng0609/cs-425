CREATE DEFINER=`root`@`localhost` TRIGGER pointsUpdate AFTER INSERT ON ForumComments
FOR EACH ROW
BEGIN
	DECLARE v_id INT;
    SET v_id = (SELECT id FROM Student WHERE userId = NEW.userId);
   UPDATE student SET points = (points+5) WHERE id = v_id;
END