CREATE DEFINER=`root`@`localhost` PROCEDURE `showModerators`()
BEGIN
SELECT * FROM moderator m
LEFT JOIN enrollmentig e
ON m.moderatorid = e.id;
END