CREATE DEFINER=`root`@`localhost` PROCEDURE `register`(
IN job				VARCHAR(20),
IN firstName		VARCHAR(20),
IN lastName			VARCHAR(40),
IN email			VARCHAR(320),
IN joinedYear		INT,
IN password			VARCHAR(320))
BEGIN
	DECLARE v_id INT;
    CALL loginGenerator(firstName, lastName, joinedYear, password, @out_value);
    SET v_id = @out_value;
	INSERT INTO Users VALUES(v_id, firstName, lastName, email, UPPER(job));
END