
SELECT UNIQUE(phone)
FROM FamilyPackage f
INNER JOIN RecCenterMember r
ON r.family_id = f.id
WHERE l_name = 'O''Shea';

SELECT f_name, l_name, title
FROM Instructor i
INNER JOIN Class c
ON i.id = c.instructor;

SELECT f_name, l_name
FROM RecCenterMember
WHERE family_id is NULL;

SELECT f_name, l_name
FROM RecCenterMember r
INNER JOIN Enrollment e
ON e.member_id = r.id
INNER JOIN Class c
ON e.class_id = c.id
WHERE type = 'Craft' or type = 'Art';

SELECT UNIQUE(f_name), l_name
FROM RecCenterMember r
INNER JOIN Enrollment e
ON e.member_id = r.id
INNER JOIN Class c
ON e.class_id = c.id
WHERE year < 2009 
OR (year = 2009 AND season = 'Spring');

SELECT f_name, l_name, description
FROM Instructor i
INNER JOIN Class c
ON i.id = c.instructor
INNER JOIN Type t
ON t.type = c.type
WHERE (season = 'Fall' and year = 2009) 
AND c.instructor IN
(SELECT instructor
FROM Class 
WHERE season = 'Spring' and year = 2009);

SELECT UNIQUE(t.type), description
FROM Class c
INNER JOIN Type t
ON t.type = c.type
WHERE year = 2008 or year = 2009;