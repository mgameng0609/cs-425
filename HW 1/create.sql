CREATE TABLE Type(
type            VARCHAR2(20)      PRIMARY KEY,
description     VARCHAR2(80)      NOT NULL);

CREATE TABLE FamilyPackage(
id              NUMBER(4)         PRIMARY KEY,
address         VARCHAR2(20)      NOT NULL,
phone           VARCHAR2(20)      UNIQUE);

CREATE TABLE RecCenterMember(
id              NUMBER(4)         PRIMARY KEY,
f_name          VARCHAR2(20)      NOT NULL,
l_name          VARCHAR2(20)      NOT NULL,
dob             DATE              NOT NULL,
family_id       REFERENCES        FamilyPackage(id));

CREATE TABLE Instructor(
id              NUMBER(4)         PRIMARY KEY,
f_name          VARCHAR2(20)      NOT NULL,
l_name          VARCHAR2(20)      NOT NULL,
member_id       REFERENCES        RecCenterMember(id));

CREATE TABLE Class(
id              NUMBER(4)         PRIMARY KEY,
title           VARCHAR2(20)      NOT NULL,
type            REFERENCES        Type(type)        NOT NULL,
instructor      REFERENCES        Instructor(id)    NOT NULL,
season          VARCHAR2(20)      NOT NULL,
year            NUMBER(4)         NOT NULL);

CREATE TABLE Enrollment(
class_id        NOT NULL          REFERENCES        Class(id),
member_id       NOT NULL          REFERENCES        RecCenterMember(id),
cost            NUMBER(4)         NOT NULL),
CONSTRAINT pkc_enrollment PRIMARY KEY(class_id, member_id);









