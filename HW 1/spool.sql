PHONE              
--------------------
312-555-9403        


F_NAME               L_NAME               TITLE              
-------------------- -------------------- --------------------
Monica               Knapp                Needle points       
Annie                Heard                Photography         
April                O'Shea               Woodworking         
Annie                Heard                Chinese (Intro.)    
Annie                Heard                Team games          
Monica               Knapp                Yoga (Intro.)       
April                O'Shea               Origami (Adv.)      
James                Robertson            Oil painting        
Annie                Heard                Yoga (Adv.)         
James                Robertson            Chinese (Intro.)    

 10 rows selected 

F_NAME               L_NAME             
-------------------- --------------------
Vijay                Gupta               
Harry                Smith               
Lisa                 Brown               
Laura                Dickinson           
Emily                Citrin              
Cassandra            McDonald            
Leslie               Blackburn           
Sandra               Svoboda             

 8 rows selected 


F_NAME               L_NAME             
-------------------- --------------------
Mike                 O'Shea              
April                O'Shea              
Harry                Tang                
Harry                Tang                
Dongmei              Tang                
Laura                Dickinson           
Victor               Garcia              
Emily                Citrin              
Maria                Garcia              
Leslie               Blackburn           
Sandra               Svoboda             

F_NAME               L_NAME             
-------------------- --------------------
Abby                 Smith               
Abby                 Smith               

 13 rows selected 


F_NAME               L_NAME             
-------------------- --------------------
Lisa                 Tang                
Leslie               Blackburn           
Abby                 Smith               
Harry                Tang                
Dongmei              Tang                
Victor               Garcia              
Vijay                Gupta               
April                O'Shea              
Sandra               Svoboda             

 9 rows selected 

F_NAME               L_NAME
-------------------- --------------------
DESCRIPTION                                                                    
--------------------------------------------------------------------------------
April                O'Shea               
Knitting, sewing, etc.                                                          



TYPE
--------------------
DESCRIPTION
--------------------------------------------------------------------------------
Craft                
Knitting, sewing, etc.                                                          

Art                  
Painting, sculpting, etc.                                                       

Kids                 
Courses geared towards children 13 and younger                                  


TYPE
--------------------
DESCRIPTION
--------------------------------------------------------------------------------
Languages            
Anything to do with writing, literature, or communication                       

Exercise             
Any courses having to do with physical activity                                 


