<?php session_start();
include 'home.php'; ?>
<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <link rel="stylesheet" type="text/css" href="style.css">
        <title></title>
    </head>
    <body>
        <table border="1">
            <thead>
                <tr>
                    <th>Forum Search Results</th>
                </tr>
            </thead>
            <tbody>
        <?php
        $forumSearch = DB::getInstance()->forumSearch($_POST['forumSearch']);
        while($row = mysqli_fetch_assoc($forumSearch)){
           echo "<tr><td>";
           echo DB::getInstance()->linkToForumSearchResults($row['title'], $row['interestGroup']);
           echo "<br>";
           echo $row['userId']." ".$row['comments'];
           echo "</td><tr>";
           
        }
        // put your code here
        ?>
            </tbody>
        </table>        
    </body>
</html>
