<?php session_start();
include 'home.php'; ?>
<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <link rel="stylesheet" type="text/css" href="style.css">
        <title>Assign</title>
    </head>
    <body>
        <?php
        $studentInfo = DB::getInstance()->students();
        $courses = DB::getInstance()->courses();
        $users = DB::getInstance()->users();
            if($_SERVER['REQUEST_METHOD'] == 'GET'){
              if($_GET['assign'] == 'ASSIGN TA'){
                echo '<form method="POST">';
                echo '<select name ="students">';
                while($row = mysqli_fetch_assoc($studentInfo)){
                  echo "<option>".$row['id']." - ".$row['firstName']." ".$row['lastName']."</option>";
                }
                echo '</select>';
                echo '<select name="courses">';
                while($row = mysqli_fetch_assoc($courses)){
                  echo "<option>".$row['id']." - ".$row['courseType'].$row['courseNumber']."</option>";
                }
                echo '</select>';
                echo '<input type="submit" value="ASSIGN TA" name = "assign" />';  
                echo '</form>';
              }
              if($_GET['assign'] == 'ASSIGN MOD'){
                echo '<form method="POST">';
                echo '<select name ="users">';
                while($row = mysqli_fetch_assoc($users)){
                  echo "<option>".$row['id']." - ".$row['firstName']." ".$row['lastName']."</option>";
                }
                echo '</select>';
                echo '<select name="courses">';
                while($row = mysqli_fetch_assoc($courses)){
                  echo "<option>".$row['id']." - ".$row['courseType'].$row['courseNumber']."</option>";
                }
                echo '</select>';
                echo '<input type="submit" value="ASSIGN MOD" name = "assign" />';  
                echo '</form>';
              }
            }
            if($_SERVER['REQUEST_METHOD'] == 'POST'){
                if($_GET['assign'] == 'ASSIGN TA'){
                $course = explode(" - ", $_POST['courses']);
                $courseId = $course[0];
                $courseName = $course[1];
                $typeAndNumber = explode(" ",$courseName);
                $courseType = $typeAndNumber[0];
                $courseNumber = $typeAndNumber[1];
                $student = explode(" - ", $_POST['students']);
                $studentId = $student[0];
                $studentName = $student[1];
                $firstAndLast = explode(" ", $studentName);
                $firstName = $firstAndLast[0];
                $lastName = $firstAndLast[1];
                DB::getInstance()->assignTA($courseId, $studentId);

              }
              if($_GET['assign'] == 'ASSIGN MOD'){
                echo '<br>';
                $course = explode(" - ", $_POST['courses']);
                $courseId = $course[0];
                $courseName = $course[1];
                $user = explode(" - ", $_POST['users']);
                $userId = $user[0];
                $userName = $user[1];
                $assignMod = DB::getInstance()->assignMod($userId, $courseId);
                echo $userName." is now a moderator of ".$courseName."<br>";
                echo "Here are the list of "."<a href='moderator.php'>moderators</a>";
              }
            }
        ?>
        
    </body>
</html>
