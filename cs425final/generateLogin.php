<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <link rel="stylesheet" type="text/css" href="style.css">
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
            require_once 'Database.php';
            $job = DB::getInstance()->convertHTML($_GET["job"]);
            $semester = DB::getInstance()->convertHTML($_GET["semester"]);
            $degreeType = DB::getInstance()->convertHTML($_GET["degreeType"]);
            $firstName = DB::getInstance()->convertHTML($_GET["firstName"]);
            $lastName = DB::getInstance()->convertHTML($_GET["lastName"]);
            $email = DB::getInstance()->convertHTML($_GET["email"]);
            $yearJoined = DB::getInstance()->convertHTML($_GET["yearJoined"]);
            $password = DB::getInstance()->convertHTML($_GET["password"]);
            $confirmPassword = DB::getInstance()->convertHTML($_GET["confirmPassword"]);
            if (strcmp($password,$confirmPassword) !== 0) {
                exit('Passwords do not match');
            }
            $result = DB::getInstance()->loginGenerator($job,$firstName,$lastName,$email,$yearJoined,$password);
            echo "Your username is ";
            ?>
        <strong><?php 
        $username = strtolower($firstName[0]).strtolower($lastName).$yearJoined;
        echo $username;
        $userId = DB::getInstance()->getUserId($firstName,$lastName);
        if($job == 'Student'){
          echo 'student';
          $studentInsert = DB::getInstance()->student($yearJoined,$semester,$degreeType,$userId);
        } else {
          echo 'professor';
          $profInsert = DB::getInstance()->professor($yearJoined, $degreeType, $userId);
        }
        ?></strong><br>
        <form action="index.php">
            <input type="submit" value="Login" name="Login" />
        </form>
    </body>
</html>
